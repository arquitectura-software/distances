package mx.itesm.finalarquitectura.distancematrix.validation;

import mx.itesm.finalarquitectura.distancematrix.domain.DistanceMatrixResult;
import mx.itesm.finalarquitectura.distancematrix.domain.DistanceRequest;
import mx.itesm.finalarquitectura.distancematrix.domain.LatLng;
import mx.itesm.finalarquitectura.distancematrix.pojo.Message;
import mx.itesm.finalarquitectura.distancematrix.pojo.Result;

public class DistanceRequestValidation {

    public static Result<DistanceMatrixResult> validate(DistanceRequest distanceRequest) {
        Result<DistanceMatrixResult> result = new Result<>();
        result.setErrorCode(null);

        String message = "";

        if(distanceRequest == null) {
            result.setErrorCode(400);
            message += "El cuerpo del post no puede ser nulo. ";
        } else {
            if(distanceRequest.getOrigin() == null) {
                result.setErrorCode(400);
                message += "El origen no puede ser nulo. ";
            } else {
                if(distanceRequest.getOrigin().getLatitude() == null || distanceRequest.getOrigin().getLongitude() == null) {
                    result.setErrorCode(400);
                    message += "Se debe incluir latitud y longitud para el origen. ";
                }
            }

            if(distanceRequest.getDestinations() == null || distanceRequest.getDestinations().length == 0) {
                result.setErrorCode(400);
                message += "Se deben incluir los destinos. ";
            } else {
                for(LatLng currentDestination : distanceRequest.getDestinations()) {
                    if(currentDestination == null || currentDestination.getLatitude() == null || currentDestination.getLongitude() == null) {
                        result.setErrorCode(400);
                        message += "Los destinos no tienen la información completa. ";
                        break;
                    }
                }
            }
        }

        result.setMessage(new Message(message));
        return result;
    }
}
