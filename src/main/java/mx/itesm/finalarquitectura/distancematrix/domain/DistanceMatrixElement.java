package mx.itesm.finalarquitectura.distancematrix.domain;

public class DistanceMatrixElement {
    public DistanceMatrixElementObject distance;
    public DistanceMatrixElementObject duration;
    public String status;
}
