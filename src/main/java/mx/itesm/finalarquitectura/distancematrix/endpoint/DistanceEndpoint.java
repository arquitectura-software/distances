package mx.itesm.finalarquitectura.distancematrix.endpoint;

import mx.itesm.finalarquitectura.distancematrix.domain.DistanceMatrixResult;
import mx.itesm.finalarquitectura.distancematrix.domain.DistanceRequest;
import mx.itesm.finalarquitectura.distancematrix.pojo.Message;
import mx.itesm.finalarquitectura.distancematrix.pojo.Result;
import mx.itesm.finalarquitectura.distancematrix.service.DistanceMatrixService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Component
@Path("")
@Produces(MediaType.APPLICATION_JSON)
public class DistanceEndpoint {

    @Autowired
    private DistanceMatrixService distanceMatrixService;

    @POST
    @Path("/distance")
    public Response getDistancesByOrigin(DistanceRequest distanceRequest) {
        Result<DistanceMatrixResult> result = distanceMatrixService.getDistances(distanceRequest);
        Response response;
        if(result.getData().isPresent()) {
            response = Response.ok(result.getData().get()).build();
        } else {
            if(result.getErrorCode() != null && result.getErrorCode() > 0) {
                response = Response.status(result.getErrorCode()).entity(result.getMessage()).build();
            } else {
                response = Response.serverError().entity(new Message("Error de servidor.")).build();
            }
        }
        return response;
    }
}
