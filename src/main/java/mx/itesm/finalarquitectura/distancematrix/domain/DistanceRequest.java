package mx.itesm.finalarquitectura.distancematrix.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

import java.util.Arrays;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class DistanceRequest {

    private LatLng origin;
    private LatLng[] destinations;

    public LatLng getOrigin() {
        return origin;
    }

    public void setOrigin(LatLng origin) {
        this.origin = origin;
    }

    public LatLng[] getDestinations() {
        return destinations;
    }

    public void setDestinations(LatLng[] destinations) {
        this.destinations = destinations;
    }

    @Override
    public String toString() {
        return "DistanceRequest{" +
                "origin=" + origin +
                ", destinations=" + Arrays.toString(destinations) +
                '}';
    }
}
