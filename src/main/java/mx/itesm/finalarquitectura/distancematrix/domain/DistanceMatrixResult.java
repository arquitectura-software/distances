package mx.itesm.finalarquitectura.distancematrix.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class DistanceMatrixResult {
    public DistanceMatrixRow[] rows;
    public String status;
    public String error_message;
}
