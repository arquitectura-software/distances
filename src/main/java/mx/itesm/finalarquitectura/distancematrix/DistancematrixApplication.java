package mx.itesm.finalarquitectura.distancematrix;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@SpringBootApplication
@EnableDiscoveryClient
public class DistancematrixApplication {

	public static void main(String[] args) {
		SpringApplication.run(DistancematrixApplication.class, args);
	}
}
