package mx.itesm.finalarquitectura.distancematrix.service;

import com.google.gson.Gson;
import mx.itesm.finalarquitectura.distancematrix.domain.ApiKey;
import mx.itesm.finalarquitectura.distancematrix.domain.DistanceMatrixResult;
import mx.itesm.finalarquitectura.distancematrix.domain.DistanceRequest;
import mx.itesm.finalarquitectura.distancematrix.domain.LatLng;
import mx.itesm.finalarquitectura.distancematrix.pojo.Result;
import mx.itesm.finalarquitectura.distancematrix.validation.DistanceRequestValidation;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.io.BufferedReader;
import java.io.Reader;
import java.util.Optional;
import java.util.concurrent.TimeUnit;

@Service
public class DistanceMatrixService {

    private OkHttpClient client;


    @Value("${distancematrix.api.url}")
    private String distanceMatrixApiUrl;

    @Value("${apikeys.url}")
    private String apiKeysServerUrl;

    private String currentApiKey;

    private static final Logger logger = LoggerFactory.getLogger(DistanceMatrixService.class);

    @PostConstruct
    private void initService() {
        this.client = new OkHttpClient.Builder()
                .connectTimeout(20, TimeUnit.SECONDS)
                .writeTimeout(20, TimeUnit.SECONDS)
                .readTimeout(20, TimeUnit.SECONDS)
                .build();
    }

    public Result<DistanceMatrixResult> getDistances(DistanceRequest distanceRequest) {
        Result<DistanceMatrixResult> result = DistanceRequestValidation.validate(distanceRequest);

        if(result.getErrorCode() != null && result.getErrorCode() > 0) {
            return result;
        }

        result.setData(getDistancesByOrigin(distanceRequest.getOrigin(), distanceRequest.getDestinations()));
        return result;
    }

    private Optional<DistanceMatrixResult> getDistancesByOrigin(LatLng origin, LatLng[] destinations) {
        if(currentApiKey == null) {
            this.currentApiKey = getApiKey();
        }
        Optional<DistanceMatrixResult> result = Optional.empty();

        boolean obtainedData = false;
        while(!obtainedData) {
            result = getDistancesByOrigin(origin, destinations, this.currentApiKey);
            if(result.isPresent()) {
                obtainedData = true;
            } else {
                String newApiKey = getApiKey();
                if(newApiKey.equals(this.currentApiKey)) {
                    obtainedData = true;
                } else {
                    this.currentApiKey = newApiKey;
                }
            }
        }
        return result;
    }

    private Optional<DistanceMatrixResult> getDistancesByOrigin(LatLng origin, LatLng[] destinations, String apiKey) {
        String url = this.distanceMatrixApiUrl;
        url += String.format("&origins=%f,%f&destinations=", origin.getLatitude(), origin.getLongitude());

        StringBuilder stringDestinationsBuilder = new StringBuilder();
        for(int i = 0; i < destinations.length; i++) {
            if(i==0) {
                stringDestinationsBuilder.append(String.format("%f,%f", destinations[i].getLatitude(), destinations[i].getLongitude()));
            } else {
                stringDestinationsBuilder.append(String.format("|%f,%f", destinations[i].getLatitude(), destinations[i].getLongitude()));
            }
        }

        url +=  String.format("%s&key=%s", stringDestinationsBuilder.toString(), apiKey);

        Response response;

        try {
            logger.info("Requesting url: " + url);
            Request request = new Request.Builder()
                    .url(url)
                    .get()
                    .build();
            response = client.newCall(request).execute();

            if(response.code() >= 200 && response.code() < 300) {
                Reader in = response.body().charStream();
                BufferedReader reader = new BufferedReader(in);

                DistanceMatrixResult results = new Gson().fromJson(reader, DistanceMatrixResult.class);
                reader.close();
                if(results == null) {
                    throw new Exception("Error parsing JSON: DistanceMatrixRequest");
                }
                if(!results.status.equals("OK") || results.rows.length < 1 || results.rows[0].elements.length != destinations.length) {
                    throw new Exception(results.error_message + ": " + results.status + ", url: " + url);
                }

                return Optional.of(results);
            } else {
                throw new Exception("Wrong status code: DistanceMatrixRequest");
            }
        } catch (Exception e) {
            logger.error(e.getMessage() + ": " + apiKey);
        }

        return Optional.empty();
    }

    private String getApiKey() {
        ApiKey apiKey = null;
        boolean obtainedApiKey = false;
        while(!obtainedApiKey) {
            Response response;
            try {
                Request request = new Request.Builder()
                        .url(apiKeysServerUrl)
                        .get()
                        .build();
                response = client.newCall(request).execute();
                if(response.code() >= 200 && response.code() < 300) {
                    Reader in = response.body().charStream();
                    BufferedReader reader = new BufferedReader(in);

                    apiKey = new Gson().fromJson(reader, ApiKey.class);
                    reader.close();
                    if(apiKey == null) {
                        throw new Exception("Error parsing JSON: ApiKey request");
                    } else {
                        obtainedApiKey = true;
                    }
                }
            } catch (Exception e) {
                logger.error(e.getMessage());
            }
        }
        return apiKey.getKey();
    }
}
